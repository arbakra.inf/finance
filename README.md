# Finance at GitLab

This is the primary repository of the Finance team at GitLab.

# Quick Links

### Finance Operations & Planning

##### Planning
* Headcount Planning
* Non-Headcount Planning

##### Investor Updates
* Investor Updates

##### Customer Solutions
* ProServe Model
* ProServe SOW Milestones
* Bonus Payout Model

## Accounting

Coming soon....

## Finance Operations & Planning

Finance Operations & Planning works directly with the CFO and executive team leaders across the company to enhance and improve GitLab's forecasting and business modeling capabilities. Finance Operations & Planning also works with GitLab's Data & Analytics team to help develop predictive modeling and decision support capabilities. We ultimately ensure our financial planning is healthy, profitable and aligns with business objectives. 

For more information regarding Finance Operations & Planning, please visit our [Handbook](https://about.gitlab.com/handbook/finance/financial-planning-process/)

### Financial Modeling @ GitLab

#### Key Points:

- Always start with a **MVM** (Minimum Viable Model)
- Focus on operational finance vs accounting finance
	- It's easy to project out revenue and cost, yet more important to build a model that projects out key metrics to drive business decisons
- Make it easily audible
- Metrics change over time. As a business changes, ensure the metrics do as well
	
	
#### What Investors Look for in a Financial Model

 - Create a model Investors understand
 - Consistent Formatting
 - Bring Key Assumptions in one location 
 - Create Overall Summaries
 - Put KPI's up front and make them clear
 - Proof that the vision is viable and the numbers show it

#### How to Present a Model to Investors

 - Use financial slides to tell a story
 - Make the headline clear and up front
 - Create a graph vs an income statement of text growth and call out key points
 
#### Lessons Learned

 - Leaving out or making key KPI's difficult to find
 - Never hardcode assumptions as it makes it very hard to audit
 - Label models well
 - Create a flow for a financial model

#### Determine How Your Business Operates  
 
-  Ensure forecasted budgets are followed up with on actual expenses to see how the business or departments did

#### Advice

- Create the story for our business
- Explain the business model in one paragraph. Explain the problem being solved and how GitLab solves it. Then explain how GitLab spends money, acquires users and earns revenue
- List out GitLab's major cost items. Staff offices, travel, equipment, inventory, etc. This is a key place to start when modeling. 
- Ultimately, customers are the only relevant judges of your business model

## Investor Relations

Coming soon....

## Data Analytics 

Finance works closely with the [Data & Analytics](https://gitlab.com/meltano/analytics) team. For more information regarding Data & Analytics at GitLab, please visit the [Data & Analytics repo](https://gitlab.com/meltano/analytics).
The Data & Analytics team is a part of the Finance organization within GitLab, but serves the entire company. The Data & Analytics maintains a data warehouse where information from all business operations is stored and managed for analysis.

## Principles

Coming soon....

## Priorities

Like the rest of the company, we set quarterly objectives and key results. These are available on our company [OKR](https://about.gitlab.com/okrs/) page.

## Contributing to Finance

We welcome contributions and improvements, please see the [contribution guidelines](CONTRIBUTING.md).

# License

This code is distributed under the MIT license, see the [LICENSE](LICENSE) file.