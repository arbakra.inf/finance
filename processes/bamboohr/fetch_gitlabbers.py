from PyBambooHR import PyBambooHR
import pandas as pd
import warnings

warnings.filterwarnings('ignore')

#Authenticate
bamboo = PyBambooHR.PyBambooHR(subdomain='', api_key='')

#Fetch Custom Report created in BambooHR
bamboo_dict = bamboo.request_company_report(243)

#Create DataFrame and format/filter dataframe
df = pd.DataFrame(bamboo_dict['employees'])
active_gitlabbers = df[df['status'] == 'Active']
active_gitlabbers['usd_salary'] = pd.to_numeric(active_gitlabbers['4379.0'].map(lambda x: x.lstrip('+-').rstrip(' USD')))
current_gitlabbers = active_gitlabbers[['fullName1','hireDate','customCostCenter','customRole','division','department','jobTitle','usd_salary']]

current_gitlabbers