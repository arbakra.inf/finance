
## Cost of Good Sold Accounts
<details open>
<summary>Cost of Good Sold Accounts</summary>
<br>

|   GL account number   |   GL account name             | 
|------|------------------------------------------------| 
| 5000 | 30% facility for incoming employees COGS       | 
| 5001 | 30% facility withholding COGS                  | 
| 5002 | Accounting COGS                                | 
| 5003 | Airfare COGS                                   | 
| 5004 | Amortization COGS                              | 
| 5005 | Audit & Tax COGS                               | 
| 5007 | Bad Debt Expense COGS                          | 
| 5008 | Bank Fees COGS                                 | 
| 5009 | Benefits Medical and Other COGS                | 
| 5010 | Benefits - Fringe COGS                         | 
| 5011 | Bonus COGS                                     | 
| 5013 | Business Meals & Entertainment COGS            | 
| 5014 | Commissions COGS                               | 
| 5015 | Company Functions COGS                         | 
| 5016 | Computer & Office Equipment - Expensed COGS    | 
| 5017 | Consulting fees COGS                           | 
| 5020 | Credit Card Transaction Fees COGS              | 
| 5021 | Professional dues, membership fees COGS        | 
| 5022 | Employee Relocation COGS                       | 
| 5023 | Employee Training Cost of Sales                | 
| 5025 | Exchange rate differences COGS                 | 
| 5026 | Hosting Services COGS                          | 
| 5027 | Hotels & Lodging COGS                          | 
| 5028 | Gifts & Donations COGS                         | 
| 5029 | Holiday Allowance COGS                         | 
| 5031 | Internet COGS                                  | 
| 5033 | Insurance - Business COGS                      | 
| 5034 | Insurance - Vehicle COGS                       | 
| 5037 | Legal COGS                                     | 
| 5038 | Legal COGS : General Corporate COGS            | 
| 5039 | Legal COGS : Offering Costs COGS               | 
| 5040 | Office Supplies COGS                           | 
| 5041 | Meals - Company Provided COGS                  | 
| 5042 | Other Expenses COGS                            | 
| 5045 | Paid Holiday Allowance COGS                    | 
| 5046 | Parking, Gas, Tolls, Mileage COGS              | 
| 5047 | Payroll Service Fee COGS                       | 
| 5048 | Payroll Tax COGS                               | 
| 5049 | Postage & Shipping COGS                        | 
| 5050 | Printing & Copying COGS                        | 
| 5054 | Internal Recruiting COGS                       | 
| 5055 | Recruiting Fees COGS                           | 
| 5056 | Rent or Lease COGS                             | 
| 5057 | R&D allowance on wages (S&O) COGS              | 
| 5058 | Salaries and Wages COGS                        | 
| 5059 | Salaries and Wages COGS : Contractors COGS     | 
| 5060 | Software Subscriptions COGS                    | 
| 5062 | Taxes & License COGS                           | 
| 5063 | Taxis, Car Service, Public Transportation COGS | 
| 5064 | Telephone COGS                                 | 
| 5065 | Training & Development COGS                    | 
| 5066 | Utilities COGS                                 | 
| 5067 | Workers Compensation COGS                      | 
| 5071 | Team Building COGS                             | 
| 5072 | Depreciation - COGS                            | 
| 5073 | Licenses & Registrations COGS                  | 
| 5074 | Other Travel  Cost of Sales                    | 
| 5075 | Referral Fees - COGS                           | 
| 5076 | Coworking Space COGS                           | 
| 5077 | Stock Compensation COGS                        | 
| 5079 | Intercompany COGS 1                            | 
</details>

## Expense Accounts
<details open>
<summary>Expense Accounts</summary>
<br>

|   GL account number   |   GL account name             | 
|------|---------------------------------------------------| 
| 6000 | 30% facility for incoming employees               | 
| 6000 | Expenses                                          | 
| 6001 | 30% facility withholding                          | 
| 6002 | Accounting                                        | 
| 6003 | Airfare                                           | 
| 6004 | Amortization                                      | 
| 6005 | Audit & Tax                                       | 
| 6007 | Bad Debt Expense                                  | 
| 6008 | Bank Fees                                         | 
| 6009 | Benefits Medical and Other                        | 
| 6010 | Benefits - Fringe                                 | 
| 6011 | Bonus                                             | 
| 6013 | Business Meals & Entertainment                    | 
| 6014 | Commissions                                       | 
| 6015 | Company Functions                                 | 
| 6016 | Computer & Office Equipment - Expensed            | 
| 6017 | Consulting fees                                   | 
| 6020 | Credit Card Transaction Fees                      | 
| 6021 | Professional dues, membership fees                | 
| 6022 | Employee Relocation                               | 
| 6023 | Employee Training                                 | 
| 6025 | Exchange rate differences                         | 
| 6026 | Hosting Services                                  | 
| 6027 | Hotels & Lodging                                  | 
| 6028 | Gifts & Donations                                 | 
| 6029 | Holiday Allowance                                 | 
| 6031 | Internet                                          | 
| 6033 | Insurance - Business                              | 
| 6034 | Insurance - Vehicle                               | 
| 6037 | Legal                                             | 
| 6038 | Legal : General Corporate                         | 
| 6039 | Legal : Offering Costs                            | 
| 6040 | Office Supplies                                   | 
| 6041 | Meals - Company Provided                          | 
| 6042 | Other Expenses                                    | 
| 6045 | Paid Holiday Allowance                            | 
| 6046 | Parking, Gas, Tolls, Mileage                      | 
| 6047 | Payroll Service Fee                               | 
| 6048 | Payroll Tax                                       | 
| 6049 | Postage & Shipping                                | 
| 6050 | Printing & Copying                                | 
| 6054 | Internal Recruiting                               | 
| 6055 | Recruiting Fees                                   | 
| 6056 | Rent or Lease                                     | 
| 6057 | Research and development allowance on wages (S&O) | 
| 6058 | Salaries and Wages                                | 
| 6059 | Salaries and Wages : Contractors                  | 
| 6060 | Software Subscriptions                            | 
| 6062 | Taxes & License                                   | 
| 6063 | Taxis, Car Service, Public Transportation         | 
| 6064 | Telephone                                         | 
| 6065 | Training & Development                            | 
| 6066 | Utilites                                          | 
| 6067 | Workers Compensation                              | 
| 6071 | Team Building                                     | 
| 6072 | Depreciation                                      | 
| 6073 | Licenses & Registrations                          | 
| 6074 | Other Travel                                      | 
| 6075 | Referral Fees                                     | 
| 6076 | Coworking Space                                   | 
| 6077 | Stock Compensation Expense                        | 
| 6100 | Marketing Programs                                | 
| 6110 | Marketing Programs : Marketing Site               | 
| 6120 | Marketing Programs : Demand Advertising           | 
| 6130 | Marketing Programs : Field Events                 | 
| 6140 | Marketing Programs : Email                        | 
| 6150 | Marketing Programs : Brand                        | 
| 6160 | Marketing Programs : Prospecting                  | 
</details>

