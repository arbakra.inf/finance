## 4 Quarter Department Planning Process
This issue serves as a checklist to ensure that all tasks associated with **GitLab's Rolling 4 Quarter Forecast** is accounted for and accurate. 

## Send Out Invites
On the first Monday on the last month in the quarter, budget review invites need to be sent out to division leaders. Invites should be between the 17th and - 20th. 

### Engineering

* [ ] - VP of Product [Job]
* [ ] - Head of Product [Mark]
* [ ] - VP of Engineering [Eric]

### G&A
* [ ] - CFO [Paul]
* [ ] - CCO [Barbie]
* [ ] - Director of Data & Analytics [Walter] 
* [ ] - Director of Recruiting [Kevin] 
* [ ] - Director of Legal [Jamie]

### Sales
* [ ] - CRO & VP of Sales [McB & Chad]
* [ ] - Director of Customer Success [Kristen]

### Marketing
* [ ] - CMO [N/A]
* [ ] - Director of Product Marketing [Ashish]
* [ ] - Director of Demand Gen [AT]
* [ ] - Director of Corporate Marketing [N/A]
* [ ] - Director of Field Marketing [Leslie]
* [ ] - Director of Marketing Ops [LJ]

### Cost of Sales
* [ ] - Director of ProServe [Brendan]
* [ ] - Director of Support [Tom]

### Other
* [ ] - Director of Cloud Natives [Priyanka] 
* [ ] - Director of Partnerships [Eliran] 
* [ ] - VP of Alliances [Brandon]

## Review Non Headcount & Headcount Budgets
During the budget reviews, division leaders should be expected to have an outline of the next rolling four quarters non-headcount & headcount expenses. 

- The non-headcount expense spreadsheet can be requested from Finance
- Headcount expenses should be mocked up by the division leaders and discussed during the budget review. If a division head needs a template, he or she can refer to the template [here](https://docs.google.com/spreadsheets/d/13-S9q5DPwFhSUHMrQkqyTS0d-hYmb6cy87zw4Qt7dR0/edit?usp=sharing)

## Dates
* [ ] - 1st Monday on the last month in the quarter: Send Invites
* [ ] - 1st Tuesday on the last month in the quarter: Make 4 quarter forecast annoucement during team call
* [ ] - 14th - 17th of Month: Budget Reviews
* [ ] - 22nd of Month: Finalize Budgets with @pmachle
* [ ] - 22nd of Month: Take Snapshots of headcount & non-headcount expenses
* [ ] - 22nd of Month: Meet with @artNasser to load in new budgets to NetSuite

## Templated Invites

```
Hi, 

This is an invite and reminder to prepare for the upcoming rolling four quarter forecast review. 
During our meeting we will review your forecasted non-headcount and headcount expenses. 

All non-headcount expenses can be added and reviewed here: 


All headcount expenses can be added and reviewed here:


Current Budget vs Actuals can be found here:


Please visit the Finance repo to log an issue or learn more about the [process](https://gitlab.com/gitlab-com/finance)

Let me know if you have any questions!
```

