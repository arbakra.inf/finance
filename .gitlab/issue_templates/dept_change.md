## New Department Checklist

#### FP&A
* [ ]  Create a merge request with updated department [here](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/source/handbook/finance/department-structure/index.html.md)
* [ ]  Document the mapped changes in a google sheet so that everyone is on the same page

#### PeopleOps
* [ ]  Update BambooHR to reflect changes
* [ ]  Remove antiquated department (if applicable)
* [ ]  Add new department and which employees are moving to new department

#### Payroll
* [ ]  Update ADP
* [ ]  Remove antiquated department (if applicable)
* [ ]  Add new department and which employees are moving to new department

#### Accounting
* [ ]  Update NetSuite to reflect the ADP changes
* [ ]  Remove antiquated department (if applicable)
* [ ]  Add new department and which employees are moving to new department
* [ ]  Ensure allocation for new departments are understood and allocated properly

@artNasser, @wilsonlau, @chloe, @jnguyen28