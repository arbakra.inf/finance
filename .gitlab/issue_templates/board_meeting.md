# Quarterly Board Meeting Checklist

This issue serves as a checklist for gathering and adding board specific information for GitLab's Quarterly Board Meeting. Review Old Board Decks to ensure consistency in how we are putting slides together. These can be found in the `Board` subfolder in Google Drive

## Important Dates


## Tasks
* [ ] Review slide deck and update appropriately based on slides
* [ ] Update 5 year forecast in Revenue Model
 - Use [Mendoza](https://techcrunch.com/2018/02/09/understanding-the-mendoza-line-for-saas-growth/) formula
  - Mendoza Line Formula = 82% * Growth Rate of Prior Year
* [ ] Update 5 Year forecast in Financial Model
* [ ] Update 2 year quarterly forecast
* [ ] Update Product P&L 
 - Enterprise, GitLab.com, GitHost
 - Revenue, Cost of Sales, etc.....
* [ ] Add GitLab.com Metrics
* [ ] Add Looker & Churn Analysis to slide deck 
* [ ] Update 'Changes' Tab in Financial Model
* [ ] Check with @pmachle for any additional items needed
* [ ] Archive Non-Headcount, Rolling 4 Quarter, Financial, & Revenue Model in `Planning` subfolder. Label as `20xx-Qx Planning`