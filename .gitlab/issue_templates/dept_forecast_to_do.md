## 4 Quarter Forecast - Department To Do Checklist 

This issue serves as a checklist to ensure that all tasks associated with **GitLab's Rolling 4 Quarter Forecast** is completed by each department head in a consistent manner. The #business-channel slack channel will be where a majority of communication will happen. Ensure you join the channel for any updates or news. 

**Note: The department checklist needs to be completed before the Rolling 4 Quarter Forecast towards the middle of the month. Failure to do so will slow down the planning process**

## Department Checklist
 
* [ ] Joined #business-planning channel
* [ ] Received `Rolling 4 Quarter Forecast` Invite
* [ ] Developed a planning process that works for you and your team 
* [ ] Sent invites to managers to review their needs well before the `Rolling 4 Quarter Forecast` meeting
* [ ] Added Non-Headcount Expenses for Next 4 Quarters 
* [ ] Added Current Headcount & Forecasted Headcount to your headcount spreadsheet. If you do not have a headcount spreadsheet you can use a template [here](https://docs.google.com/spreadsheets/d/13-S9q5DPwFhSUHMrQkqyTS0d-hYmb6cy87zw4Qt7dR0/edit?usp=sharing)

## Important Dates [in order of process]
1st Monday on the last month in the quarter: Send Invites
14th - 17th on the last month in the quarter: Budget Reviews
22nd on the last month in the quarter: Budgets are Finalized
26th on the the first month in the new quarter: Budgets are reviewed / approved by Board
30th on the the first month in the new quarter: New Budgets are released to department heads

Questions?? slack @chase on the #business-planning channel
