## Vendor Contract Process
This templated issue serves as a base for routing vendor contracts through the GitLab approval workflow. 
## Step 1

- Attached the **UNSIGNED** vendor contract
## Step 2

- Provide the total cost: 

**Note: Contracts above $50k need CEO approval. Contracts above $100k need Board approval. - please ping Sid on Slack to ensure he sees the issue if the contract is over 50K. Please refer to the [Authorization Matrix](https://about.gitlab.com/handbook/finance/authorization-matrix/) for guidance.**

## Step 3
- Request approval from functional leader. If functional leader approves, resume to **Step 4**

##### Functional Approval:

- [ ] (tag functional leader here)
## Step 4

- The required approvers below need to sign off for full approval. If all required approvers approve, resume to **Step 5**

##### Budget Approval:

- [ ] @wwright (ensures Cost Center & GL Account is captured. If the expense is GL 6060, Biz Ops is tagged in comment)

##### Security Approval:

- [ ] @kathyw (checks any PII concerns with vendor)

##### Legal Approval:
- [ ] @mkshams

## Step 5

- Send contract to @pmachle for signature

## Step 6
 
 - Upload to Contract to ContractWorks. Need Access to Contract Works?....Request it [here](https://gitlab.com/gitlab-com/access-requests/issues/new?issuable_template=New%20Access%20Request)

## Step 7

- Close Issue! Please!
**** 

cc: @melissa3, @llamb, @cnunez, @wzabaglio, @jhurewitz, @kstithem
/assign @wwright @mkshams @jhurewitz
/confidential
/label ~"Vendor Contract" ~Procurement